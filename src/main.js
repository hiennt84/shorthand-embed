var MobileDetect = require('mobile-detect');
var md = new MobileDetect(window.navigator.userAgent);

var EmbedContent = function (config){
    this.id = (new Date()).getTime();
    this.config = config;
    this.init();
}

EmbedContent.prototype.init = function (){
    this.calculateSize()
        .attachMobileDetectObject()
        .setTemplate();
    
    this.createIframe();
    this.setIframeContent();
}

EmbedContent.prototype.createIframe = function (){
    var htmlCode = '<iframe title="CTA image" src="" allowtransparency="true" frameborder="0" \
        marginheight="0px" marginwidth="0px" \
        allow="geolocation; microphone; camera" \
        name="' + this.id + '" id="' + this.id + '" \
        style="width: '+ this.config.data.actualWidth +';display: block; overflow: hidden;height:' + this.config.data.actualHeight + '; border: none;" scrolling="no"></iframe>';
    
    document.write(htmlCode);

    this.iframe = document.getElementById(this.id);

    var tmp_is_ie = !!window.ActiveXObject;
    if (tmp_is_ie === true) {
        try {
            var iframe = this.iframe;
            var doc = iframe.contentDocument ? iframe.contentDocument : (iframe.contentWindow.document || iframe.document);
            doc.open();
            doc.write("");
        } catch (err) {
            this.iframe.src = "javascript:void((function(){document.open();document.domain=\'" + this.getBaseDomain() + "\';document.close();})())";
        }
    }
}   

EmbedContent.prototype.calculateSize = function (){
    var config = this.config;

    // Caculate the width, height depend on devices
    var actualWidth = md.tablet() && config.data.tablet && config.data.tablet.width 
    ? config.data.tablet.width 
    : (md.mobile() && config.data.mobile && config.data.mobile.width ? config.data.mobile.width : config.data.width );

    var actualHeight = md.tablet() && config.data.tablet && config.data.tablet.height 
        ? config.data.tablet.height 
        : (md.mobile() && config.data.mobile && config.data.mobile.height ? config.data.mobile.height : config.data.height );

    config.data.actualWidth = actualWidth;
    config.data.actualHeight = actualHeight;

    // Set the config
    this.config = config;

    // Chaining method
    return this;
}

EmbedContent.prototype.attachMobileDetectObject = function (){
    // Pass the MobileDetect object to the twig template, it is very helpful
    // to deal with variant devices
    this.config.data.md = md;

    return this;
}

EmbedContent.prototype.setTemplate = function (){
    var template = require("./templates/"+ this.config.template +".html.twig");
    this.template = template(this.config.data);
    return this;
}

EmbedContent.prototype.getBaseDomain = function () {
    var thn = window.location.hostname;
    var cc = 0;
    var buff = "";
    for (var i = 0; i < thn.length; i++) {
        var chr = thn.charAt(i);
        if (chr == ".") {
            cc++;
        }
        if (cc == 0) {
            buff += chr;
        }
    }
    if (cc == 2) {
        thn = thn.replace(buff + ".", "");
    }
    return thn;
}

EmbedContent.prototype.setIframeContent = function (){
    var iframe = this.iframe;
    var doc = iframe.contentDocument ? iframe.contentDocument : (iframe.contentWindow.document || iframe.document);
    doc.open();
    doc.write(this.template);
    setTimeout(function () {
        doc.close();
    }, 200);
}

window.ShortHandEmbed = window.ShortHandEmbed || EmbedContent;